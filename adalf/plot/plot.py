import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd

def plot_confidence_matrix(arr, width, height, fmt):
    df_cm = pd.DataFrame(arr, index = range(2), columns = range(2))
    plt.figure(figsize=(width, height))
    sns.heatmap(df_cm, annot=True, fmt = fmt)
    plt.title("Confusion matrix")
    plt.ylabel("True class")
    plt.xlabel("Predicted class")
    plt.show()

def plot_train_loss(train_loss, val_loss, epochs, width = 10, height = 6):
    plt.figure(1, figsize=(width, height))
    plt.plot(range(epochs), train_loss)
    plt.plot(range(epochs), val_loss)
    plt.ylabel("Loss")
    plt.xlabel("Epoch")
    plt.title("Training vs Validation Loss")
    plt.grid(True)
    plt.legend(["Training", "Validation"])
    plt.show()

def plot_train_acc(train_acc, val_acc, epochs, width = 10, height = 6):
    plt.figure(1, figsize=(width, height))
    plt.plot(range(epochs), train_acc)
    plt.plot(range(epochs), val_acc)
    plt.ylabel("Accuracy")
    plt.xlabel("Epoch")
    plt.title("Training vs Validation Accuracy")
    plt.grid(True)
    plt.legend(["Training", "Validation"])
    plt.show()

def plot_test_roc(fpr, tpr, roc_auc, model_name):
    plt.figure()
    plt.plot([0, 1], [0, 1], "k--")
    plt.plot(fpr, tpr, label="{} (area = {:.3f})".format(model_name, roc_auc))
    plt.xlabel("False positive rate")
    plt.ylabel("True positive rate")
    plt.title("ROC Curve")
    plt.legend(loc="best")
    plt.grid()
    plt.show()

def plot_test_recall(precision, recall, prec_rec_auc, model_name):
    plt.figure()
    plt.plot([0, 1], [1, 0], 'k--')
    plt.plot(recall, precision, label='{} (area = {:.3f})'.format(model_name, prec_rec_auc))
    plt.xlabel('Precision')
    plt.ylabel('Recall')
    plt.title('Precision-recall curve')
    plt.legend(loc='best')
    plt.grid()
    plt.show()


def plot_test_recall(precision, recall, prec_rec_auc, model_name):
    plt.figure()
    plt.plot([0, 1], [1, 0], 'k--')
    plt.plot(recall, precision, label='{} (area = {:.3f})'.format(model_name, prec_rec_auc))
    plt.xlabel('Precision')
    plt.ylabel('Recall')
    plt.title('Precision-recall curve')
    plt.legend(loc='best')
    plt.grid()
    plt.show()


def plot_scores_per_class(width, height, test_scores, train_scores, separated, bins, th, y, y_train):

    # Split into signal and background
        signal_test_scores = test_scores[(y.flatten() == 1)]
        bg_test_scores = test_scores[(y.flatten() == 0)]
        signal_train_scores = train_scores[(y_train.flatten() == 1)]
        bg_train_scores = train_scores[(y_train.flatten() == 0)]
        x1 = len(list(signal_train_scores))
        x2 = len(list(bg_train_scores))
        x3 = len(list(signal_test_scores))
        x4 = len(list(bg_test_scores))
        # Plot
        if separated:
            fig, (ax1, ax2) = plt.subplots(1, 2)
            fig.set_size_inches(width, height)
            #La integral bajo cada curva es = 1
            n1,bins1, patches = ax1.hist(signal_train_scores,log=True, density= True,alpha = 0.5, bins=bins,  label=f"Signal (training), Elements = {x1}") 
            n2,bins2, patches = ax2.hist(bg_train_scores,log=True,density= True, alpha = 0.5, bins=bins, label=f"BG (training), Elements = {x2}")

            n1,bins1, patches = ax1.hist(signal_test_scores,log=True,density= True, alpha = 0.5, bins=bins, label=f"Signal (testing), Elements = {x3}")
            n2,bins2, patches = ax2.hist(bg_test_scores,log=True,density= True, alpha = 0.5, bins=bins, label=f"BG (testing), Elements = {x4}")


            ax1.legend(loc='best'); ax2.legend(loc='best')
            ax1.set_xlabel('Score');ax2.set_xlabel('Score')
            ax1.set_ylabel('Frequency');ax2.set_ylabel('Frequency')
        
        else:
            fig = plt.figure()
            fig.set_size_inches(width, height)
            signal_train_scores[signal_train_scores>=1]=0.99
            print("signal_test_scores2", signal_train_scores)
            signal_test_scores[signal_test_scores>=1]=0.99
            print("signal_test_scores",signal_test_scores)
            bg_train_scores[bg_train_scores>=1]=0.99
            print("bg_train_scores",bg_train_scores)
            bg_test_scores[bg_test_scores>=1]=0.99
            print("bg_test_scores",bg_test_scores)
            plt.hist(signal_train_scores,log=True, alpha = 0.4,density = True,color = 'darkblue', bins=bins,label=f"Signal (training), Elements = {x1}")
            plt.hist(bg_train_scores,log=True, alpha = 0.4,density = True,color='darkred', bins=bins,label=f"BG (training), Elements = {x2}")
            plt.hist(bg_test_scores,log=True, alpha = 0.6,density = True, histtype= 'step',linewidth=2,hatch='/', color = 'red', bins=bins,label=f"BG (testing), Elements = {x4}")            
            plt.hist(signal_test_scores,log=True, alpha = 0.6,density = True, histtype= 'step',linewidth=2, hatch='/',color = 'blue', bins=bins,label=f"Signal (testing), Elements = {x3}")

            plt.vlines(th, 0 ,20, color='black', linestyles='solid', linewidth=3 ,label=f'Threshold = {th}')

            plt.legend(loc='best')
            plt.xlabel('Score')
            plt.ylabel('Frequency')
            return signal_test_scores, bg_test_scores, signal_train_scores, bg_train_scores
        plt.show()



import pandas as pd
import json
import numpy as np
#import tensorflow as tf
#abstract
from abc import ABC, abstractmethod

#keras
from keras.models import Sequential
from keras.layers import Input, Dense, Activation, Dropout
#from keras.models import Adagrad as adagrad, Adam as adam, Adamax as adamax
from keras.optimizers import Adagrad as adagrad, Adam as adam, Adamax as adamax
from keras.models import Model
from keras.models import model_from_json
#clear
from keras.backend import clear_session
import gc

#sklearn
from sklearn.metrics import (classification_report, confusion_matrix, roc_curve, roc_auc_score,
precision_recall_curve, average_precision_score)
from sklearn.utils import shuffle
from sklearn.metrics import f1_score

#plot
from matplotlib.colors import ListedColormap
import matplotlib.pyplot as plt
import seaborn as sns
from ada.plot import plot_train_loss, plot_test_roc, plot_test_recall
#os
import os


class KerasModel(ABC):
    """Mother class for keras models"""
    def __init__(self, n_input):
        self.model = None
        self.history = None
        self.title = ""
        self.model_name = ""

        # Training
        #Added by GG on Jul 2021
        self.X_train = None
        self.y_train = None
        self.w_train = None
    
    def fit(self, X_train, y_train, w_train, X_val, y_val, w_val, epochs, class_weights = {0:1, 1:1}, verbose = 1):
        ####
        #Added by GG Jul 2021
        self.X_train = X_train
        self.y_train = y_train
        ####
        if self.model is not None:
            self.history = pd.DataFrame(self.model.fit(
                X_train.values, y_train, sample_weight = w_train,
                epochs = epochs,
                verbose = verbose,
                validation_data = (X_val.values, y_val, w_val),
                class_weight = class_weights,
            ).history)
        else:
            print("Build your model first!")
    
    def plot_loss(self, width = 10, height = 6):
        loss = self.history['loss']
        val_loss = self.history['val_loss']
        plot_train_loss(loss, val_loss, len(loss), width = 10, height = 6)
    
    def save(self, directory, version):
        if self.model is not None:
            #save model
            model_json = self.model.to_json()
            with open(f"{directory}/{self.model_name}_{version}.json", "w") as json_file:
                json_file.write(model_json)
            #save weights
            self.model.save_weights(f"{directory}/{self.model_name}_{version}.h5")
        if self.history is not None:
            #save training data
            with open(f"{directory}/{self.model_name}_{version}.csv", mode='w') as f:
                self.history.to_csv(f)
    
    def load(self, directory, version, model_name = None):
        if model_name is not None:
            self.model_name = model_name
        # load json and create model
        if self.model is None:
            json_file = open(f"{directory}/{self.model_name}_{version}.json", 'r')
            loaded_model_json = json_file.read()
            json_file.close()
            self.model = model_from_json(loaded_model_json)
        #load weights
        self.model.load_weights(f"{directory}/{self.model_name}_{version}.h5")
        #load train history
        self.history = pd.read_csv(f"{directory}/{self.model_name}_{version}.csv")
    
    #added on Jun 2020
    @abstractmethod
    def predict(self, x_test, th):
        print("Not implemented :(")
    
    #added on 29 Jun 2020
    def f1(self, x_test, y_test, w_test, th):
        #prediction
        y_pred = self.predict(x_test, th)
        #f1
        f1_by_class = f1_score(y_test, y_pred, sample_weight = w_test, average = None)
        f1_wavg = f1_score(y_test, y_pred, sample_weight = w_test, average='weighted')
        return f1_by_class.tolist() + [f1_wavg, ]
    
    #added on Jun 2020
    def complete_evaluation(self, x_test, y_test, w_test, th, save = False, dest_path = ".", name = "v1"):
        #prediction
        y_pred = self.predict(x_test, th)

        #class report
        class_report = classification_report(y_test, y_pred, output_dict = True)
        weighted_class_report = classification_report(y_test, y_pred, output_dict = True, sample_weight=w_test)

        #accuracy
        acc = class_report["accuracy"]
        del class_report["accuracy"]
        weighted_acc = weighted_class_report["accuracy"]
        del weighted_class_report["accuracy"]

        #confusion matrix
        cm = confusion_matrix(y_test, y_pred).tolist()
        weighted_cm = confusion_matrix(y_test, y_pred, sample_weight=w_test).tolist()

        #all the evaluations
        complete_eval = {
            "class_report": class_report,
            "weighted_class_report": weighted_class_report,
            "accuracy": acc,
            "weighted_accuracy": weighted_acc,
            "cm": cm,
            "weighted_cm": weighted_cm,
        }

        #save evaluation
        if save:
            json_eval = json.dumps(complete_eval)
            with open(f"{dest_path}/eval_{self.model_name}_{name}.json", 'w') as json_file:
                json_file.write(json_eval)

        return complete_eval

    #Added on june 2021. Based on John's code
    def best_significance_th(self, X, y, ths = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9]):
        significances = [self.significance(X, y, th) for th in ths]
        idx = np.argmax(significances)
        return (ths[idx], significances[idx])

    def best_f1_th(self, X, y, w = None):
        # Precision-recall
        scores = self.model.predict(X)
        try:
            if w is not None:
                precision, recall, thresholds = precision_recall_curve(y, scores, sample_weight = w)
            else:
                precision, recall, thresholds = precision_recall_curve(y, scores)
            # Calculate f1 scores
            fscores = (2 * precision * recall) / (precision + recall)
            # Return optimal f1
            idx = np.argmax(fscores)
            return (thresholds[idx], fscores[idx])
        except:
            return (0, 0)

    def f1_b(self, X, y, threshold, w = None):
        scores = self.model.predict(X).flatten()
        y_pred = (scores > threshold).astype(int)
        if w is not None:
            return f1_score(y, y_pred, average=None, sample_weight=w)
        return f1_score(y, y_pred, average=None)

class Autoencoder(KerasModel):
    def __init__(self, n_input, anomaly_class):
        super().__init__(n_input)
        self.anomaly_class = anomaly_class

    def fit(self, x_train, w_train, x_val, w_val, epochs):
        super().fit(x_train, x_train, w_train, x_val, x_val, w_val, epochs)
    
    #added on Jun 2020
    def plot_reconstruction_error(self, x_test, y_test, th):
        x_pred = self.model.predict(x_test)
        errors = np.mean(np.power(x_test.values - x_pred, 2), axis=1)
        color = ListedColormap(["#1f85ad", "#ff7b00"])
        plt.figure(figsize=(14, 10))
        plt.scatter(x = range(len(errors)), y = errors, c = y_test.flatten(), s=16, cmap = color)
        plt.axhline(y=th, color='red', linestyle='-')
        plt.show()
    
    #added on Jun 2020
    def predict(self, x_test, th):
        x_pred = self.model.predict(x_test)
        errors = np.mean(np.power(x_test.values - x_pred, 2), axis=1)
        
        if self.anomaly_class == 1:
            y_pred = (errors > th).astype(int)
        else:
            y_pred = (errors <= th).astype(int)
            
        return y_pred
    
    #added on Jun 2020
    def plot_confidence_matrix(self, x_test, y_test, th, fmt):
        #prediction
        y_pred = self.predict(x_test, th)
        #confidence matrix
        conf_matrix = confusion_matrix(y_test.flatten(), y_pred)
        #plot
        plt.figure(figsize=(8, 8))
        sns.heatmap(conf_matrix, annot=True, fmt=fmt)
        plt.title("Confusion matrix")
        plt.ylabel('True class')
        plt.xlabel('Predicted class')
        plt.show()

class BinaryClassifier(KerasModel):

    def __init__(self, n_input):
        super().__init__(n_input)

    def evaluate_with_weights(self, X_test, y_test, w_test, threshold = 0.4):
        y_pred_prob = self.model.predict(X_test)
        y_pred = (y_pred_prob > threshold) #sobre que valor será true

        print("Classification Report")
        print(classification_report(y_test, y_pred, sample_weight = w_test))
        print("Confussion Matrix")
        print(confusion_matrix(y_test, y_pred, sample_weight = w_test))
    
    def evaluate(self, X_test, y_test, threshold = 0.4):
        y_pred_prob = self.model.predict(X_test)
        y_pred = (y_pred_prob > threshold)

        print("Classification Report")
        print(classification_report(y_test, y_pred))
        print("Confussion Matrix")
        print(confusion_matrix(y_test, y_pred))

    def plot_roc(self, X_test, y_test):
        y_scores = self.model.predict(X_test).ravel()
        fpr, tpr, _ = roc_curve(y_test, y_scores)
        roc_auc = roc_auc_score(y_test, y_scores)
        plot_test_roc(fpr, tpr, roc_auc, self.model_name)
    
    def plot_recall(self, X_test, y_test):
        y_scores = self.model.predict(X_test).ravel()
        precision, recall, _ = precision_recall_curve(y_test, y_scores)
        prec_rec_auc = average_precision_score(y_test, y_scores)
        plot_test_recall(precision, recall, prec_rec_auc, self.model_name)
    
    def predict(self, x_test, th):
        y_pred_prob = self.model.predict(x_test)
        y_pred = (y_pred_prob > th).astype(int)
        return y_pred

    #####################
    #Taken from framework.py at https://github.com/Jneror/ATLASImbalanceLearning/tree/LossFunctions
    #Added by GG on Jul 2021

    def plot_score_histogram(self, X, y, bins=64, width = 8, height = 8, separated=False,
    X_train = None, y_train=None):
        # Check training sets
        if X_train is None or y_train is None:
            if self.X_train is None or self.y_train is None:
                print("No training sets!")
                return
            X_train = self.X_train
            y_train = self.y_train
        # Calculate scores
        test_scores = self.model.predict(X).flatten()
        train_scores = self.model.predict(X_train).flatten()
        # Split into signal and background
        signal_test_scores = test_scores[(y.flatten() == 1)]
        bg_test_scores = test_scores[(y.flatten() == 0)]
        signal_train_scores = train_scores[(y_train.flatten() == 1)]
        bg_train_scores = train_scores[(y_train.flatten() == 0)]
        # Plot
        if separated:
            fig, (ax1, ax2) = plt.subplots(1, 2)
            fig.set_size_inches(width, height)
            ax1.hist(signal_train_scores, alpha = 0.5, bins=bins, label="Signal (training)")
            ax2.hist(bg_train_scores, alpha = 0.5, bins=bins, label="BG (training)")
            ax1.hist(signal_test_scores, alpha = 0.5, bins=bins, label="Signal (testing)")
            ax2.hist(bg_test_scores, alpha = 0.5, bins=bins, label="BG (testing)")
            ax1.legend(loc='best'); ax2.legend(loc='best')
            ax1.set_xlabel('Score');ax2.set_xlabel('Score')
            ax1.set_ylabel('Frequency');ax2.set_ylabel('Frequency')
        else:
            fig = plt.figure()
            fig.set_size_inches(width, height)
            plt.hist(signal_train_scores, alpha = 0.5, bins=bins, label="Signal (training)")
            plt.hist(bg_train_scores, alpha = 0.5, bins=bins, label="BG (training)")
            plt.hist(signal_test_scores, alpha = 0.5, bins=bins, label="Signal (testing)")
            plt.hist(bg_test_scores, alpha = 0.5, bins=bins, label="BG (testing)")
            plt.legend(loc='best')
            plt.xlabel('Score')
            plt.ylabel('Frequency')
        plt.show()

##############
### Models ###
##############

class BinClassifModelV1(BinaryClassifier):

    def __init__(self, n_input):
        #model
        self.model = Sequential()
        #input
        self.model.add(Dense(32, input_dim = n_input, kernel_initializer='uniform',activation='softplus'))
        #hidden layers
        self.model.add(Dropout(rate=0.2))
        self.model.add(Dense(64, kernel_initializer='he_uniform', activation='softplus'))
        self.model.add(Dropout(rate=0.2))
        self.model.add(Dense(128, kernel_initializer='he_uniform', activation='softplus'))
        self.model.add(Dropout(rate=0.2))
        self.model.add(Dense(256, kernel_initializer='he_uniform', activation='softplus'))
        self.model.add(Dropout(rate=0.2))
        self.model.add(Dense(128, kernel_initializer='he_uniform', activation='softplus'))
        self.model.add(Dropout(rate=0.2))
        self.model.add(Dense(64, kernel_initializer='he_uniform', activation='softplus'))
        self.model.add(Dropout(rate=0.2))
        self.model.add(Dense(32, kernel_initializer='he_uniform', activation='softplus'))
        self.model.add(Dropout(rate=0.2))
        self.model.add(Dense(1, kernel_initializer='he_uniform', activation='sigmoid'))
        #compile
        self.model.compile(optimizer=adagrad(lr=0.05), loss='binary_crossentropy')

        #title
        self.title = 'optimizer: adagrad , lr = 0.05, loss = binary crossentropy'

        #training
        self.history = None

        #name
        self.model_name = "BCM1"

class AutoencoderModelV1(Autoencoder):

    def __init__(self, n_features, anomaly_class):

        #input
        input_layer = Input(shape=(n_features, ))

        #encode
        encoder = Dense(8,kernel_initializer='he_uniform',activation='relu')(input_layer)
        drop = Dropout(rate=0.2)(encoder)

        #latent
        latent = Dense(2, kernel_initializer='he_uniform',activation='relu')(drop)
        drop = Dropout(rate=0.2)(latent)

        #decode
        decoder = Dense(8, kernel_initializer='he_uniform',activation='relu')(drop)
        drop = Dropout(rate=0.2)(decoder)

        #output
        output_layer = Dense(n_features, activation="sigmoid")(drop)
        self.model = Model(inputs=input_layer, outputs=output_layer)

        #compile
        self.model.compile(loss='mean_squared_error', optimizer=adam(lr=0.01))

        #title
        self.title = 'optimizer: adam , lr = 0.01, loss = mean squared error'

        #training
        self.history = None

        #name
        self.model_name = "AM1"

        #anomaly class
        self.anomaly_class = anomaly_class

class BinClassifModel2(BinaryClassifier):

    def __init__(self, n_input, lr, opti, acti, drop):
        #model
        self.model = Sequential()
        #input
        self.model.add(Dense(32, input_dim = n_input, kernel_initializer='uniform',activation=acti))
        #hidden layers
        self.model.add(Dropout(rate=drop))
        self.model.add(Dense(64, kernel_initializer="he_uniform", activation=acti))
        self.model.add(Dropout(rate=drop))
        self.model.add(Dense(128, kernel_initializer="he_uniform", activation=acti))
        self.model.add(Dropout(rate=drop))
        self.model.add(Dense(256, kernel_initializer="he_uniform", activation=acti))
        self.model.add(Dropout(rate=drop))
        self.model.add(Dense(128, kernel_initializer="he_uniform", activation=acti))
        self.model.add(Dropout(rate=drop))
        self.model.add(Dense(64, kernel_initializer="he_uniform", activation=acti))
        self.model.add(Dropout(rate=drop))
        self.model.add(Dense(32, kernel_initializer="he_uniform", activation=acti))
        self.model.add(Dropout(rate=drop))
        self.model.add(Dense(1, kernel_initializer="he_uniform", activation='sigmoid'))
        #compile
        self.model.compile(optimizer=opti(lr=lr), loss='binary_crossentropy')

        #training
        self.history = None

        #name
        self.model_name = "BCM2"

class BinClassifModel3(BinaryClassifier):

    ##GG: added loss_f on june 2021

    def __init__(self, n_input, lr, opti, acti,loss_f='binary_crossentropy'):
        #model
        self.model = Sequential()
        #input
        self.model.add(Dense(32, input_dim = n_input, kernel_initializer='uniform',activation=acti))
        #hidden layers
        self.model.add(Dropout(rate=0.2))
        self.model.add(Dense(64, kernel_initializer="he_uniform", activation=acti))
        self.model.add(Dropout(rate=0.2))
        self.model.add(Dense(128, kernel_initializer="he_uniform", activation=acti))
        self.model.add(Dropout(rate=0.2))
        self.model.add(Dense(256, kernel_initializer="he_uniform", activation=acti))
        self.model.add(Dropout(rate=0.2))
        self.model.add(Dense(128, kernel_initializer="he_uniform", activation=acti))
        self.model.add(Dropout(rate=0.2))
        self.model.add(Dense(64, kernel_initializer="he_uniform", activation=acti))
        self.model.add(Dropout(rate=0.2))
        self.model.add(Dense(32, kernel_initializer="he_uniform", activation=acti))
        self.model.add(Dropout(rate=0.2))
        self.model.add(Dense(1, kernel_initializer="he_uniform", activation='sigmoid'))
        #compile modified
        self.model.compile(optimizer=opti(lr=lr), loss=loss_f)

        #training
        self.history = None

        #name
        self.model_name = "BCM3"

# Created on SPOOKY month 2020
class FeedForwardNeuralNetworkArchitecture(BinaryClassifier):

    def __init__(self, n_features, lr, opti, acti):

        # Input
        input_layer = Input(shape=(n_features, ))

        # Hidden layers
        layer = Dense(32, kernel_initializer='uniform',activation=acti)(input_layer)
        drop = Dropout(rate=0.2)(layer)
        layer = Dense(64, kernel_initializer='he_uniform',activation=acti)(input_layer)
        drop = Dropout(rate=0.2)(layer)
        layer = Dense(128, kernel_initializer='he_uniform',activation=acti)(input_layer)
        drop = Dropout(rate=0.2)(layer)
        layer = Dense(256, kernel_initializer='he_uniform',activation=acti)(input_layer)
        drop = Dropout(rate=0.2)(layer)
        layer = Dense(128, kernel_initializer='he_uniform',activation=acti)(input_layer)
        drop = Dropout(rate=0.2)(layer)
        layer = Dense(64, kernel_initializer='he_uniform',activation=acti)(input_layer)
        drop = Dropout(rate=0.2)(layer)
        layer = Dense(32, kernel_initializer='he_uniform',activation=acti)(input_layer)
        drop = Dropout(rate=0.2)(layer)
        
        # Output
        output_layer = Dense(1, kernel_initializer="he_uniform", activation='sigmoid')(drop)

        # Model
        self.model = Model(inputs = input_layer, outputs = output_layer)

        # Compile
        self.model.compile(optimizer = opti(lr = lr), loss='binary_crossentropy')

        # Training
        self.history = None

        # Name
        self.model_name = "FFNNA"

# Created on SPOOKY month 2020
##GG: added loss_f on june 2021
class BinaryClassifierModel4(BinaryClassifier):
    def __init__(self, n_features, loss_f='binary_crossentropy'):
        # Input
        input_layer = Input(shape=(n_features, ))

        # Hidden layers
        layer = Dense(32, kernel_initializer='uniform',activation="relu")(input_layer)
        drop = Dropout(rate=0.2)(layer)
        layer = Dense(64, kernel_initializer='he_uniform',activation="relu")(drop)
        drop = Dropout(rate=0.2)(layer)
        layer = Dense(128, kernel_initializer='he_uniform',activation="relu")(drop)
        drop = Dropout(rate=0.2)(layer)
        layer = Dense(256, kernel_initializer='he_uniform',activation="relu")(drop)
        drop = Dropout(rate=0.2)(layer)
        layer = Dense(128, kernel_initializer='he_uniform',activation="relu")(drop)
        drop = Dropout(rate=0.2)(layer)
        layer = Dense(64, kernel_initializer='he_uniform',activation="relu")(drop)
        drop = Dropout(rate=0.2)(layer)
        layer = Dense(32, kernel_initializer='he_uniform',activation="relu")(drop)
        drop = Dropout(rate=0.2)(layer)
        
        # Output
        output_layer = Dense(1, kernel_initializer="he_uniform", activation='sigmoid')(drop)

        # Model
        self.model = Model(inputs = input_layer, outputs = output_layer)

        # Compile
        #self.model.compile(optimizer = adamax(0.05), loss='binary_crossentropy')
        self.model.compile(optimizer = adamax(0.05), loss=loss_f)
        # Training
        self.history = None

        # Name
        self.model_name = "BC4"

##############
### Tuning ###
##############

#Based on John's code

#Added on June 2021

class Hypertuner():
    def __init__(self, learning_rates, optimizers, activations, loss_functions, batch_sizes, class_weights_combs,objective):
        self.acts = activations
        self.losses = loss_functions
        self.optis = optimizers
        self.lrs = learning_rates
        self.cws = class_weights_combs
        self.batches = batch_sizes
        self.obj = objective

    #Added ub June 2021
    def get_combinations(self):
        return [(act, loss, optimizer, lr, batch_size, class_weights)
                for act in self.acts
                    for loss in self.losses
                        for optimizer in self.optis
                            for lr in self.lrs
                                for batch_size in self.batches
                                    for class_weights in self.cws]

    def bruteforce_search(self, tuning_source, epochs, X_train, X_val, X_test, y_train, y_val, y_test, w_train, w_val, w_test, metrics = [], best_in = "avg", checkpoint=0, model_test_name="BCM3"):
        assert best_in in ["0","1","avg"]
        # Generate the different combinations
        combs = self.get_combinations()
        print(len(combs), "combinations")
       
        
        #Train models with these combinations
        scores = []
        for i in range(checkpoint,len(combs)):
            # Find saved model
            #change to test other model

            if os.path.exists(f"{tuning_source}/comb{i}.h5"):
                print("Loading combination", i)
                if model_test_name == "BCM3":
                    model = BinClassifModel3(X_test.shape[1],combs[i][3], combs[i][2], combs[i][0], combs[i][1])
                else:
                    model = BinaryClassifierModel4(X_test.shape[1], combs[i][1])
                model.load(tuning_source, f"comb{i}")
                eval =  model.complete_evaluation(X_test, y_test, w_test, 0.4)
                scores.append((eval,eval["weighted_class_report"]))
            # Or train
            else:
                print("Training combination", i)
                if model_test_name == "BCM3":
                    model = BinClassifModel3(X_test.shape[1],combs[i][3], combs[i][2], combs[i][0], combs[i][1])
                else:
                    model = BinaryClassifierModel4(X_test.shape[1], combs[i][1])
                model.fit(X_train, y_train, w_train, X_val, y_val, w_val, epochs, combs[i][-1], verbose=0)
                model.save(tuning_source, f"comb{i}")
                
            # Calculate F score
            if (self.obj == "significance"):
                significance = model.best_significance_th(X_test, y_test)
                scores.append(significance)
            else:
                f1 = model.best_f1_th(X_test, y_test, w_test)
                f1_per_class = model.f1_b(X_test, y_test, f1[0], w_test)
                scores.append([f1[0], *f1_per_class, f1[1]])
            # Clear
            clear_session()
            gc.collect()
            del model

        if (self.obj == "significance"):
            scores = pd.DataFrame(scores, columns=["th","significance"])
            scores.dropna(inplace=True)
            scores.sort_values(by=["significance"], ascending=False, inplace=True)
            idx = scores["significance"].idxmax()

        else:
            scores = pd.DataFrame(scores, columns=["th","0","1","avg"])
            scores.dropna(inplace=True)
            scores.sort_values(by=[best_in], ascending=False, inplace=True)
            idx = scores[best_in].idxmax()

        best_comb = combs[idx]
        

        return combs, scores, best_comb

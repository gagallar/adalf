from keras.backend import clear_session
import gc
from ada.data import split_dataset
from ada.model import BinaryClassifierModel4 as BC4
from ada.losses import ClassBalancedSigmoidFocalCrossEntropy

seed = 420

#Added on Feb 2022
def get_loss_list(loss_function, alphas = False, gammas = False, betas = False, n_y = False):
    
    loss_list = []
    
    if loss_function == 'binary_crossentropy':
        return ['binary_crossentropy']
    
    if loss_function == 'balanced_cross_entropy':
        losses = [tfa.losses.SigmoidFocalCrossEntropy(alpha=alpha, gamma=gamma, name = f'fl_a{alpha}_g{gamma}')
        for alpha in alphas[0]
            for gamma in [1.]] 
        return losses
    
    if loss_function == 'focal_loss':
        losses = [tfa.losses.SigmoidFocalCrossEntropy(alpha=alpha, gamma=gamma, name = f'fl_a{alpha}_g{gamma}')
        for alpha in alphas[1]
            for gamma in gammas]
        return losses
    
    else: #'class_balanced_focal_loss'
        losses = [ClassBalancedSigmoidFocalCrossEntropy(alpha=alpha, gamma=gamma, beta=beta, n_y=n_y,
                                              name = f'cbfl_a{alpha}_g{gamma}_b{beta}')
        for alpha in alphas[1]
            for gamma in gammas
                for beta in betas]
        return losses


#Added on Feb 2022
def losses_list(loss_function, group_n, n_y):
    # HYPERPARAMETERS FOR LOSS FUNCTIONS USED
    alphas_nn = [0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9]  # Change one of these lists if you want to test another hyperparameters
    alphas_fl = [.25, .5, .75, 1] #For focal loss
    gammas = [0, .2, .5, 1., 2., 5.] #For focal loss
    betas = [0.9, 0.99, 0.999, 0.9999, 0.99999] #For class balanced loss
    losses = []
    alphas = [alphas_nn , alphas_fl]
    if group_n == 1:
        
        return get_loss_list(loss_function, alphas, gammas, betas)
    
    if group_n == 2:

        return get_loss_list(loss_function, alphas, gammas, betas)
        
    else: # group_n == 3
        
        get_loss_list(loss_function, alphas, gammas, betas, n_y)
        return losses
        
def combs_list(losses, df, group_n,cw, seed=seed):
       

    if group_n == 2 or group_n == 1:
        #class weights
        cws = [{0:11,1:10},{0:12,1:10},{0:13,1:10},{0:14,1:10},{0:15,1:10}]

        #dataset split proportions
        splits = [(0.6, 0.2, 0.2), (0.5, 0.3, 0.2), #80:20
                                    (0.5, 0.2, 0.3)] #70:30


        sets = [split_dataset(df, *split, seed,oversampling=True) for split in splits]

        combs= []

        if cw :
            combs = [(sets[i], splits[i], cw, loss) for i in range(len(splits))
                                                                for cw in cws
                                                                    for loss in losses]
            return combs

        else:
            combs = [(sets[i], splits[i], loss) for i in range(len(splits))
                                                    for loss in losses]
            return combs
    
    else:
        pass 
    
    return combs


def dest_path(loss_function,signal,group_n,cw): #if this function is called without argument the last path used will be the output.
    parent_dir = '../../' 
    global_storage = 'trained_models' #directory where the experiments will be storage.
    try:
        path = os.path.join(parent_dir, global_storage)
        os.mkdir(path)
        print('Directory created: ' + global_storage)
        parent_dir = parent_dir + global_storage + '/'
        print('Parent_dir set as: ' + parent_dir)
    except:
        print('Parent_dir set as: ' + parent_dir)
    
    out_path = ''

    if group_n == 1:
        out_path = 'G'+str(group_n)+'-NN-' + loss_function + '-' + signal

    if group_n == 2:
        if cw:
            out_path = 'G'+str(group_n)+'a'+'-NN-' + loss_function + '-' + signal
        else:
            out_path = 'G'+str(group_n)+'b'+'-NN-' + loss_function + '-' + signal
    try:
        path = os.path.join(parent_dir, out_path)
        os.mkdir(path)
        print('Directory created: ' + out_path)
        
        out_path = parent_dir + out_path + '/'  
    except:
        print('Directory already exists!' )
    
    return out_path


def train_models(combs, loss_function,signal, group_n, cw):
     
    out_path = dest_path(loss_function,signal, group_n, cw)
    print('Models will be storage at',out_path)    
    
    for i in range(len(combs)):
                
        if group_n == 1 or group_n == 2:
            if cw:
                #create model
                _set = combs[i][0]
                _cw =  combs[i][2]
                _loss =combs[i][3]

                _model = BC4(_set["x"]["test"].shape[1],loss_f = _loss)        
                #fit

                _model.fit(_set['x']['train'],_set['y']['train'],
                         _set['w']['train'],_set['x']['val'],_set['y']['val'],
                         _set['w']['val'],50,verbose=0, class_weights = _cw)

                #save
                _model.save(out_path,f"comb{i}")
                print(f'trained comb{i}')
            if not cw:
                #create model
                _set = combs[i][0]
                _loss =combs[i][2]

                _model = BC4(_set["x"]["test"].shape[1],loss_f = _loss)        
                #fit
                _model.fit(_set['x']['train'],_set['y']['train'],
                        _set['w']['train'],_set['x']['val'],_set['y']['val'],
                        _set['w']['val'],50,verbose=0)
                #save
                _model.save(out_path,f"comb{i}")
                print(f'trained comb{i}')
        
        else:
            pass
        
    return